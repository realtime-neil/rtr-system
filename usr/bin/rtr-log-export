#!/bin/sh

# /usr/bin/rtr-log-export

set -eu

export LC_ALL=C

this="$(readlink -f "$0")"
readonly this="${this}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"
tmpdir="$(
    mktemp -dt "${whatami}.XXXXXX" \
        | xargs -I{} sh -c "chmod 1777 {} && echo {}"
)"
readonly tmpdir="${tmpdir}"
export TMPDIR="${tmpdir}"

cleanup() {
    status="$?"
    rm -rf "${tmpdir}" || true
    return "${status}"
}
trap cleanup EXIT

log() { echo "${whatami}[$$]:" "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }
die() {
    error "$@"
    usage >&2
    exit 1
}

usage() {
    cat <<EOF

Usage: $0 [-h] [-d] [-f] [-o OUTFILE] [-- ZIP_ARG...]
Export RTR logs as a zip archive.

Options:

    -h            print this usage and return success
    -d            include available coredumps
    -f            force (overwrite existing outfile)
    -o OUTFILE    write to OUTFILE (default: ${whatami}-\$(hostname)-\$(date -u +"%Y%m%dT%H%M%SZ").zip)

Caveats:

    * The outfile is always an zip archive.

    * OUTFILE must have a .zip file extension.

    * Unsupported ZIP_ARGs:

      * inclusion list with relative path; e.g., -i@include.lst

      * exclusion list with relative path; e.g., -i@exclude.lst

Examples:

    \$ $0

    \$ $0 -d

    \$ $0 -o mylogs.zip

    \$ $0 -f -o mylogs.zip

    \$ $0 -- --exclude '*.service.log'

    \$ $0 -- --include '*.out' '*syslog*' @

    \$ $0 -- --suffixes .gz:.bz2:.xz

EOF
}

# https://www.etalabs.net/sh_tricks.html
# Rich's sh (POSIX shell) tricks
# Working with arrays
save_args() {
    for i; do printf %s\\n "$i" | sed "s/'/'\\\\''/g;1s/^/'/;\$s/\$/' \\\\/"; done
    echo " "
}

vet_outfile() {
    # shellcheck disable=SC2039,SC3043
    local result=""
    if ! result="$(readlink -f "$1")"; then
        error "bad path: $1"
        return 1
    fi
    if [ "${result}" = "${result%.zip}" ]; then
        error "missing .zip extension: ${result}"
        return 1
    fi
    if [ -f "${result}" ]; then
        if [ "true" = "${requested_force:-false}" ]; then
            warning "file exists: ${result}"
        else
            error "file exists: ${result}"
            return 1
        fi
    fi
    echo "${result}"
    return 0
}

# $1 : zipfile
# rest : inpaths
zip_append() (
    # The first argument is the zip archive to which we will append.
    zipfile="$1"
    # The rest of the arguments are inpaths.
    shift
    # Append to the argument array any $zip_args.
    eval "set -- $* ${zip_args-}"
    # Verbosely append to the zipfile; print everything to stderr and nothing
    # to stdout.
    zip -r "${zipfile}" "$@" >&2
)

# for given zip file, read stdin into member; move member to given path target
#
# $1 : zipfile (MUST BE FULL PATH)
# $2 : inpath
zip_stdin_as() (
    # Change into a temporary directory. This is the reason we cannot support
    # inclusion/exclusion lists with relative paths.
    cd "$(mktemp -d)"
    # Create the (empty) inpath.
    install -D /dev/null "$2"
    # Read stdin into the inpath.
    cat >"$2"
    # Append to the zipfile the inpath.
    zip_append "$1" "$2"
)

# for given zip file, rename given path source to given path target
#
# $1 : zip file
# $2 : zip member path source
# $3 : zip member path target
zip_mv() {
    # https://stackoverflow.com/questions/29956087/stdin-into-zip-command-how-do-i-specify-a-file-name
    printf '@ %s\n@=%s\n' "$2" "$3" | zipnote -w "$1"
}

################################################################################
################################################################################
################################################################################

while getopts ":hdfo:" opt; do
    case "${opt}" in
        h)
            usage
            exit "$?"
            ;;
        d) readonly requested_coredumps="true" ;;
        f) readonly requested_force="true" ;;
        o) outfile="${OPTARG}" ;;
        :) die "Missing argument: -${OPTARG}" ;;
        \?) die "Invalid option: -${OPTARG}" ;;
    esac
done
shift "$((OPTIND - 1))"
zip_args=$(save_args "$@")

################################################################################

# Acquire (or default) the outfile path.
if [ -n "${outfile-}" ]; then
    info "given outfile: ${outfile}"
else
    outfile="${whatami}_$(hostname)_$(date -u +"%Y%m%dT%H%M%SZ").zip"
    warning "defaulted outfile: ${outfile}"
fi
# Getting to here implies "${outfile}" could be a canonical or relative path.
if ! outfile="$(vet_outfile "${outfile}")"; then
    die "FAILURE: vet_outfile ${outfile}"
fi
# Getting to here implies "${outfile}" is a canonical path.
readonly outfile="${outfile}"

################################################################################

# Create an empty zip archive.
# https://en.wikipedia.org/wiki/Zip_(file_format)#Limits
# https://stackoverflow.com/questions/29234912/how-to-create-minimum-size-empty-zip-file-which-has-22b
base64 -d >"${outfile}" <<'EOF'
UEsFBgAAAAAAAAAAAAAAAAAAAAAAAA==
EOF

################################################################################

# Add regular syslogs.
zip_append "${outfile}" /var/log/syslog* || true

################################################################################

if command -v rtr-vet-controller >/dev/null 2>&1; then
    rtr-vet-controller -t \
        | zip_stdin_as "${outfile}" "rtr-vet-controller.out" || true
else
    warning "missing command: rtr-vet-controller"
fi

################################################################################

# Add any/all service unit logs.

# > Valid unit names consist of a "name prefix" and a dot and a suffix
# > specifying the unit type. The "unit prefix" must consist of one or more
# > valid characters (ASCII letters, digits, ":", "-", "_", ".", and
# > "\"). The total length of the unit name including the suffix must not
# > exceed 256 characters. The type suffix must be one of ".service",
# > ".socket", ".device", ".mount", ".automount", ".swap", ".target",
# > ".path", ".timer", ".slice", or ".scope".
#
# -- https://www.freedesktop.org/software/systemd/man/systemd.unit.html#Description
dpkg_query_listfiles="$(mktemp -t dpkg_query_listfiles.XXXXXX)"
readonly dpkg_query_listfiles="${dpkg_query_listfiles}"
# This assumes that rapidplan and/or rtr-webapps are the only packages web
# should consider. This will fail if any argument is not an installed
# package. Let it.
dpkg-query --listfiles rapidplan rtr-webapps >"${dpkg_query_listfiles}" || true
sort -uo "${dpkg_query_listfiles}" "${dpkg_query_listfiles}"

service_file_list="$(mktemp -t service_file_list.XXXXXX)"
readonly service_file_list="${service_file_list}"
# Let this fail.
grep -Ex '/lib/systemd/system/[[:alnum:]:_.\-]{1,248}[.]service' \
    <"${dpkg_query_listfiles}" >"${service_file_list}" || true

while read -r service_file; do
    service="$(basename "${service_file}")"
    journalctl --no-pager --output=short-iso --unit="${service}" \
        | zip_stdin_as "${outfile}" "${service}.log" || true
    zip_append "${outfile}" /var/log/"${service%.service}" || true
done <"${service_file_list}"

if [ "true" = "${requested_coredumps:-false}" ]; then
    # Apport saves coredumps in /var/lib/apport/coredump, but can save "crash reports" in /var/crash
    if command -v apport-cli >/dev/null 2>&1; then
        zip_append "${outfile}" /var/crash /var/lib/apport/coredump || true
    elif sudo coredumpctl dump >/dev/null 2>&1; then
        # man 1 coredumpctl tells you how to show a given field, but it doesn't
        # tell you what's available:
        #
        # > -F FIELD, --field=FIELD
        # >     Print all possible data values the specified field takes
        # >     inmatching core dump entries of the journal.
        #
        #
        # man 1 journalctl tells you what's available, but it doesn't tell you
        # what applies to coredumpctl:
        #
        # > -N, --fields
        # >     Print all field names currently used in all entries of the
        # >     journal.
        #
        #
        # So I went looking:
        #
        #     $ journalctl --fields | grep ^COREDUMP_
        coredump_filename_list="$(mktemp -t coredump_filename_list.XXXXXX)"
        readonly coredump_filename_list="${coredump_filename_list}"
        coredumpctl --no-legend --no-pager --field=COREDUMP_FILENAME list >"${coredump_filename_list}"
        sort -uo "${coredump_filename_list}" "${coredump_filename_list}"

        # sudo dump to stdout, luser zip stdin; prune trailing `.lz4` (if it
        # exists) from each member name
        while read -r coredump_filename; do
            sudo coredumpctl dump COREDUMP_FILENAME="${coredump_filename}" \
                | zip_stdin_as "${outfile}" "${coredump_filename%.lz4}" || true
        done <"${coredump_filename_list}"
    else
        warning "FAILURE: Missing coredump handler"
    fi
fi

################################################################################

# Enumerate $outfile members.
member_list="$(mktemp -t member_list.XXXXXX)"
readonly member_list="${member_list}"

# This will fail on an "Empty zipfile." --- let it.
if ! zipinfo -1 "${outfile}" >"${member_list}"; then
    info "wrote: ${outfile}"
    # Yes, zipinfo prints its error message to stdout.
    warning "$(xargs <"${member_list}")"
    exit 0
fi

# Getting to here implies $outfile is non-empty.

# For each zip archive member, do the following:
#
# * Prune the leading '/' if it exists.
#
# * Prepend a directory component that is the $outfile basename.
#
leading_dir="$(basename "${outfile}" .zip)"
readonly leading_dir="${leading_dir}"
while read -r member; do
    zip_mv "${outfile}" "${member}" "${leading_dir}/${member#/}"
done <"${member_list}"
info "wrote: ${outfile}"
zipinfo "${outfile}" >&2

exit "$?"
