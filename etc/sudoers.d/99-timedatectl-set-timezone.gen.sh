#!/bin/sh

# rtr-system/etc/sudoers.d/99-timedatectl-set-timezone.gen.sh
#
# Generate the content for etc/sudoers.d/99-timedatectl-set-timezone and print
# it to stdout.
#
# I wrote this script because I needed a way to dynamically generate a sudoers
# file for timezonectl invocations. The generated sudoers file needs to contain
# a rule of the form
#
#     ALL ALL = /usr/bin/timedatectl set-timezone $TIMEZONE
#
# ...for all possible values of $TIMEZONE.

set -euvx

# First, print the sudoers file header.
cat <<'EOF'
# /etc/sudoers.d/99-timedatectl-set-timezone

# https://www.iana.org/time-zones

EOF

# Next, print some provenance.
cat <<EOF
# generated from $(dpkg-query --show tzdata)

EOF

# https://unix.stackexchange.com/questions/504043/how-to-list-timezones-known-to-the-system/698068#698068
#
# This is how you list timezones without systemd's timedatectl:
timezone_list="$(mktemp -ut timezone_list.XXXXXX)"
awk '/^Z/ { print $2 }; /^L/ { print $3 }' \
    </usr/share/zoneinfo/tzdata.zi \
    >"${timezone_list}"
sort -uo "${timezone_list}" "${timezone_list}"
test -s "${timezone_list}"
while read -r TIMEZONE; do
    echo "ALL ALL = /usr/bin/timedatectl set-timezone" "${TIMEZONE}"
done <"${timezone_list}"

rm "${timezone_list}"

exit "$?"
